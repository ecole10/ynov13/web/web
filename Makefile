DOCKER          = docker
DOCKER_COMPOSE  = docker-compose

EXEC_NODE       = $(DOCKER_COMPOSE) run --rm --service-ports node
SHELL_NODE		= $(EXEC_NODE) sh

NPM        		= $(EXEC_NODE) npm
NG				= $(EXEC_NODE) ng

##
##Project
##-------

build:
	@$(DOCKER_COMPOSE) pull --parallel --quiet --ignore-pull-failures 2> /dev/null
	$(DOCKER_COMPOSE) build --pull

kill:
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

install: ## Install and start the project
install: build start node_modules npm-start

clean: ## Stop the project and remove generated files
clean: kill
	rm -rf ./app/node_modules ./app/dist

reset: ## Stop and start a fresh install of the project
reset: clean install

start: ## Start the project
	$(DOCKER_COMPOSE) up -d --remove-orphans --no-recreate

stop: ## Stop the project
	$(DOCKER_COMPOSE) stop

restart: ## Restart the project
restart: stop start

##
##DOCKER
##-------

build-docker-image: ## Build angular docker image
build-docker-image:
	$(DOCKER) build -f docker/nginx/Dockerfile -t music-front-nginx:$(version) app

run-docker-image: ## Run angular docker image
run-docker-image: remove-docker-container remove-docker-image
	$(MAKE) -s build-docker-image version="latest"
	$(DOCKER) run -d -p 80:80 --name music_front_nginx music-front-nginx

remove-docker-container:
	-$(DOCKER) stop music_front_nginx
	-$(DOCKER) rm music_front_nginx

remove-docker-image:
	-$(DOCKER) rmi music-front-nginx

##
##NPM
##-------

package-lock.json: ./app/package.json
	$(NPM) update

node_modules: ./app/package-lock.json
	$(NPM) install

npm-start: ## Start webserver for developpement
npm-start:
	$(NPM) start

##
##NG
##-------

ng: ## Execute a ng command
ng:
	$(NG) $(command)

##
##Tests
##-------

tests: ## Execute Jest
tests:
	$(NG) test

##
##QA
##-------

lint: ## Execute eslint
lint:
	$(NG) lint

##
##Build
##-------

compile: ## Compile for deployment
compile:
	$(NG) build

##
##CI
##-------

ci: ## Execute CI pipeline
ci: lint test

##
##Shell
##-----

shell-node: ## Access sh node container
shell-node:
	$(SHELL_NODE)

##
##Logs
##-----

logs-node: ## Access logs node container
logs-node:
	$(DOCKER_COMPOSE) logs node

##
##Documentation
##-----

.DEFAULT_GOAL := doc
doc: ## List commands available in Makefile
doc:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: doc
