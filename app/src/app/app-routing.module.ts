import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContentLayoutComponent } from '@layout/content-layout/content-layout.component';
import { AuthLayoutComponent } from '@layout/auth-layout/auth-layout.component';
import { DashboardLayoutComponent } from '@layout/dashboard-layout/dashboard-layout.component';
import { AuthUserGuard } from '@core/guard/auth-user.guard';
import { AnonymousGuard } from '@core/guard/anonymous.guard';

const routes: Routes = [
  {
    path: '',
    component: ContentLayoutComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        loadChildren: () =>
          import('@modules/home/home.module').then(m => m.HomeModule)
      },
    ]
  },
  {
    path: '',
    component: AuthLayoutComponent,
    canActivate: [AnonymousGuard],
    children: [
      {
        path: '',
        loadChildren: () =>
          import('@modules/auth/auth.module').then(m => m.AuthModule)
      },
    ]
  },
  {
    path: '',
    component: DashboardLayoutComponent,
    canActivate: [AuthUserGuard],
    children: [
      {
        path: '',
        loadChildren: () =>
          import('@modules/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
    ]
  },
  {
    path: '**',
    redirectTo: '',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
