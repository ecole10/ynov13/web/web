import { TestBed } from '@angular/core/testing';

import { ConfirmRegisterService } from './confirm-register.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ConfirmRegisterService', () => {
  let service: ConfirmRegisterService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(ConfirmRegisterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
