import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';
import { ConfirmationToken } from '@shared/models/confirmationToken';

@Injectable({
  providedIn: 'root'
})
export class ConfirmRegisterService {

  constructor(private http: HttpClient) { }

  getConfirmToken(confirmationToken: ConfirmationToken): Observable<HttpResponse<any>> {
    return this.http.get(
      `${environment.musicApiBaseUrl}/auth/register/confirm?token=${confirmationToken.token}`,
      { observe: 'response' }
    );
  }
}
