import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';
import { UserCredentials } from '@shared/models/userCredentials';
import { JWTToken } from '@shared/models/jwtToken';
import { Role } from '@shared/enums/role';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) {
  }

  postLogin(userCredentials: UserCredentials): Observable<HttpResponse<JWTToken>> {
    return this.http.post<JWTToken>(
      `${environment.musicApiBaseUrl}/auth/login`,
      userCredentials,
      { observe: 'response' }
    );
  }

  setSession(token: JWTToken) {
    let expiresAt = new Date();
    expiresAt.setMinutes(new Date().getMinutes() + 30);

    localStorage.setItem('access_token', token.accessToken);
    localStorage.setItem('refresh_token', token.refreshToken);
    localStorage.setItem('expires_at', expiresAt.toString());
  }

  logout() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
    localStorage.removeItem('expires_at');
  }

  public isLoggedIn(): boolean {
    return this.isAccessToken() && !this.isTokenExpired();
  }

  public isLoggedInAsAdmin(): boolean {
    return this.isLoggedIn() && this.getRoles().includes(Role.ADMIN);
  }

  public isTokenExpired(): boolean {
    const expiresAt = localStorage.getItem('expires_at');

    return expiresAt
      ? new Date() > new Date(expiresAt)
      : true;
  }

  public getAccessToken(): string {
    const accessToken = localStorage.getItem('access_token');

    return accessToken
      ? accessToken
      : '';
  }

  private isAccessToken(): boolean {
    return localStorage.getItem('access_token') !== null;
  }

  private getRoles(): Array<string> {
    const accessToken = localStorage.getItem('access_token');

    return accessToken
      ? JSON.parse(atob(accessToken.split('.')[1])).roles
      : [];
  }
}
