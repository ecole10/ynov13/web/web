import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';
import { Register } from '@shared/models/register';
import { User } from '@shared/models/user';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) { }

  postCreateAccount(newUser: Register): Observable<HttpResponse<User>> {
    return this.http.post<User>(
      `${environment.musicApiBaseUrl}/auth/register`,
      newUser,
      { observe: 'response' }
    );
  }
}
