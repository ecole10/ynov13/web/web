import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { TrendsTrack } from '@shared/models/trendsTrack';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { Platform } from '@shared/enums/platform';

@Injectable({
  providedIn: 'root'
})
export class TrendsService {

  constructor(private http: HttpClient) {
  }

  getDeezerTrendsTracks(): Observable<HttpResponse<TrendsTrack[]>> {
    return this.http.get<TrendsTrack[]>(
      `${environment.musicApiBaseUrl}/platforms/trends/tracks?platform=${Platform.DEEZER}`,
      { observe: 'response' }
    );
  }

  getSpotifyTrendsTracks(): Observable<HttpResponse<TrendsTrack[]>> {
    return this.http.get<TrendsTrack[]>(
      `${environment.musicApiBaseUrl}/platforms/trends/tracks?platform=${Platform.SPOTIFY}`,
      { observe: 'response' }
    );
  }
}
