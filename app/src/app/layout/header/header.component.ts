import { Component, HostBinding, OnInit } from '@angular/core';
import { LoginService } from '@services/auth/login/login.service';
import { Router } from '@angular/router';
import { OverlayContainer } from '@angular/cdk/overlay';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  constructor(private loginService: LoginService, private router: Router) {}

  ngOnInit(): void {}

  logout() {
    this.loginService.logout();
    this.router.navigateByUrl('/');
  }

  isLoggedIn(): boolean {
    return this.loginService.isLoggedIn();
  }
}
