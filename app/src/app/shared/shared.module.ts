import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@shared/material.module';
import { SelectCountryComponent } from '@shared/components/select-country/select-country.component';
import { TrendsComponent } from '@shared/components/trends/trends.component';
import { TranslateModule } from '@ngx-translate/core';
import { DarkModeComponent } from '@shared/components/dark-mode/dark-mode.component';

@NgModule({
  declarations: [
    SelectCountryComponent,
    DarkModeComponent,
    TrendsComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    TranslateModule
  ],
  exports: [
    MaterialModule,
    SelectCountryComponent,
    DarkModeComponent,
    TrendsComponent,
  ]
})
export class SharedModule { }
