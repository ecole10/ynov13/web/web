export interface TrendsTrack {
  id: string;
  title: string;
  artists: string[];
  album: string;
  albumCover: string;
  link: string;
  preview: string;
  platform: string;
}
