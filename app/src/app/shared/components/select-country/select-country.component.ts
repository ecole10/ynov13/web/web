import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Lang } from '@shared/enums/lang';

@Component({
  selector: 'app-select-country',
  templateUrl: './select-country.component.html',
  styleUrls: ['./select-country.component.scss'],
})
export class SelectCountryComponent implements OnInit {
  constructor(public translate: TranslateService) {
    translate.addLangs([Lang.FR, Lang.EN]);
  }

  ngOnInit(): void {
    const lang = localStorage.getItem('lang');

    this.translate.currentLang = lang
      ? lang
      : this.parseLang();

    this.translate.setDefaultLang(this.translate.currentLang);
  }

  public changeLang(lang: string): void {
    this.translate.use(lang);
    localStorage.setItem('lang', lang);
  }

  private parseLang(): Lang {
    const language = navigator.language.split('-')[0];
    return language === Lang.FR ? Lang.FR : Lang.EN;
  }
}
