import { Component, OnInit } from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-dark-mode',
  templateUrl: './dark-mode.component.html',
  styleUrls: ['./dark-mode.component.scss']
})
export class DarkModeComponent implements OnInit {
  darkModeControl = new FormControl(false);

  constructor(private overlayContainer: OverlayContainer) {
  }

  ngOnInit(): void {
    const darkThemeClass = 'dark-theme';
    const overlayContainerClass = this.overlayContainer.getContainerElement().classList;
    const appEl = document.getElementById('app');

    if (localStorage.getItem(darkThemeClass) && appEl) {
      this.darkModeControl.setValue(true);
      appEl.classList.add(darkThemeClass);
      overlayContainerClass.add(darkThemeClass);
    }

    this.darkModeControl.valueChanges.subscribe(value => {
      if (appEl) {
        if (value) {
          appEl.classList.add(darkThemeClass);
          overlayContainerClass.add(darkThemeClass);
          localStorage.setItem(darkThemeClass, 'true');
        } else {
          appEl.classList.remove(darkThemeClass);
          overlayContainerClass.remove(darkThemeClass);
          localStorage.removeItem(darkThemeClass);
        }
      }
    });
  }
}
