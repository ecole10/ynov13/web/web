import { Component, Input, OnInit } from '@angular/core';
import { TrendsTrack } from '@shared/models/trendsTrack';

@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  styleUrls: ['./trends.component.scss']
})
export class TrendsComponent implements OnInit {
  @Input() trendsTracks: TrendsTrack[] = [];

  constructor() {
  }

  ngOnInit(): void {

  }

  public pauseTracksBeforePlay(currentPlayer: EventTarget | null): void {
    let players = Array.from(document.getElementsByTagName('audio'));

    if (currentPlayer instanceof HTMLAudioElement) {
      players.splice(players.indexOf(currentPlayer), 1);

      players.forEach((player: HTMLAudioElement) => {
        if (!player.paused) {
          player.pause();
        }
      });
    }
  }
}
