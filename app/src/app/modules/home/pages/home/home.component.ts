import { Component, OnInit } from '@angular/core';
import { TrendsService } from '@services/trends/trends.service';
import { TrendsTrack } from '@shared/models/trendsTrack';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  spotifyTrendsTracks: TrendsTrack[] | undefined;
  deezerTrendsTracks: TrendsTrack[] | undefined;

  constructor(private trendsService: TrendsService) { }

  ngOnInit(): void {
    this.showSpotifyTrends();
    this.showDeezerTrends();
  }

  showSpotifyTrends() {
    this.trendsService.getSpotifyTrendsTracks()
      .subscribe(resp => {
        this.spotifyTrendsTracks = [...resp.body!];
      });
  }

  showDeezerTrends() {
    this.trendsService.getDeezerTrendsTracks()
      .subscribe(resp => {
        this.deezerTrendsTracks = [...resp.body!];
      });
  }
}
