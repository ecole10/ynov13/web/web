import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './pages/home/home.component';
import { HomeRouting } from '@modules/home/home.routing';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    TranslateModule,
    CommonModule,
    SharedModule,
    HomeRouting
  ]
})
export class HomeModule {
}
