import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared/shared.module';
import { DashboardRouting } from '@modules/dashboard/dashboard.routing';
import { DashboardComponent } from './pages/dashboard/dashboard.component';

@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    TranslateModule,
    CommonModule,
    SharedModule,
    DashboardRouting
  ]
})
export class DashboardModule { }
