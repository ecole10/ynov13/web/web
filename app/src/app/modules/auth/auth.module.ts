import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterComponent } from './pages/register/register.component';
import { AuthRouting } from '@modules/auth/auth.routing';
import { LoginComponent } from './pages/login/login.component';
import { SharedModule } from '@shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { ConfirmRegisterComponent } from './pages/confirm-register/confirm-register.component';

@NgModule({
  declarations: [
    RegisterComponent,
    ConfirmRegisterComponent,
    LoginComponent
  ],
  imports: [
    TranslateModule,
    CommonModule,
    SharedModule,
    AuthRouting
  ]
})
export class AuthModule { }
