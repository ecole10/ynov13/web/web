import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { LoginService } from '@services/auth/login/login.service';
import { HttpErrorResponse, HttpStatusCode } from '@angular/common/http';
import { UserCredentials } from '@shared/models/userCredentials';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  hidePassword: boolean = true;
  submitted: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private router: Router,
    private snackBar: MatSnackBar,
    private translateService: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.loginForm.controls;
  }

  formInvalid(): boolean {
    return this.loginForm.invalid;
  }

  submit(): void {
    this.submitted = true;

    if (this.formInvalid()) {
      this.reset();
    } else {
      this.loginService.postLogin(<UserCredentials>this.loginForm.value)
        .subscribe(resp => {
          if (resp.status === HttpStatusCode.Ok && resp.body) {
            this.loginService.setSession(resp.body);
            this.router.navigateByUrl('/dashboard');
          }
        }, (resp: HttpErrorResponse) => {
          this.submitted = false;

          if (resp.status === HttpStatusCode.Unauthorized) {
            this.openMessageError(resp.error.message);
          } else {
            this.openMessageError(
              this.translateService.instant('error.server_unavailable')
            );
          }
        });
    }
  }

  reset(): void {
    this.submitted = false;
    this.loginForm.reset();
  }

  openMessageError(message: string): void {
    this.snackBar.open(message, '', {
      duration: 5000,
      panelClass: ['mat-toolbar', 'mat-warn']
    });
  }
}
