import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpErrorResponse, HttpStatusCode } from '@angular/common/http';
import { ConfirmRegisterService } from '@services/auth/confirm-register/confirm-register.service';
import { TranslateService } from '@ngx-translate/core';
import { ConfirmationToken } from '@shared/models/confirmationToken';

@Component({
  selector: 'app-confirm-register',
  templateUrl: './confirm-register.component.html',
  styleUrls: ['./confirm-register.component.scss']
})
export class ConfirmRegisterComponent implements OnInit {
  confirmRegisterForm: FormGroup;
  uuidV4Pattern: RegExp = /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i;

  constructor(
    private formBuilder: FormBuilder,
    private confirmRegisterService: ConfirmRegisterService,
    private router: Router,
    private snackBar: MatSnackBar,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit(): void {
    this.confirmRegisterForm = this.formBuilder.group({
      token: ['', [Validators.required, Validators.pattern(this.uuidV4Pattern)]],
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.confirmRegisterForm.controls;
  }

  formInvalid(): boolean {
    return this.confirmRegisterForm.invalid;
  }

  submit(): void {
    if (this.formInvalid()) {
      this.reset();
    } else {
      this.confirmRegisterService.getConfirmToken(<ConfirmationToken>this.confirmRegisterForm.value)
        .subscribe(resp => {
        }, (resp: HttpErrorResponse) => {
          switch (resp.status) {
            case HttpStatusCode.NotFound:
              if (resp.error.message) {
                this.openMessageError(resp.error.message);
              } else {
                // 302 response status is converted in 404
                this.router.navigateByUrl('/login');
              }
              break;
            case HttpStatusCode.UnprocessableEntity:
              this.openMessageError(resp.error.message);
              this.router.navigateByUrl('/login');
              break;
            default:
              this.openMessageError(this.translateService.instant('error.server_unavailable'));
              break;
          }
        });
    }
  }

  reset(): void {
    this.confirmRegisterForm.reset();
  }

  openMessageError(message: string): void {
    this.snackBar.open(message, '', {
      duration: 5000,
      panelClass: ['mat-toolbar', 'mat-warn']
    });
  }
}
