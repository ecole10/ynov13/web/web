import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegisterService } from '@services/auth/register/register.service';
import { Register } from '@shared/models/register';
import { HttpErrorResponse, HttpStatusCode } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  hidePassword: boolean = true;
  submitted: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private registerService: RegisterService,
    private router: Router,
    private snackBar: MatSnackBar,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.maxLength(100)]],
      email: ['', [Validators.required, Validators.email, Validators.maxLength(50)]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(128)]]
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.registerForm.controls;
  }

  formInvalid(): boolean {
    return this.registerForm.invalid;
  }

  submit(): void {
    this.submitted = true;

    if (this.formInvalid()) {
      this.reset();
    } else {
      this.registerService.postCreateAccount(<Register>this.registerForm.value)
        .subscribe(resp => {
          if (resp.status === HttpStatusCode.Created) {
            this.router.navigateByUrl('/confirm-register');
          }
        }, (resp: HttpErrorResponse) => {
          this.submitted = false;

          if (resp.status === HttpStatusCode.UnprocessableEntity) {
            this.openMessageError(resp.error.message)
          } else {
            this.openMessageError(
              this.translateService.instant('error.server_unavailable')
            )
          }
        });
    }
  }

  reset(): void {
    this.submitted = false;
    this.registerForm.reset();
  }

  openMessageError(message: string): void {
    this.snackBar.open(message, '', {
      duration: 5000,
      panelClass: ['mat-toolbar', 'mat-warn']
    })
  }
}
