module.exports = {
  moduleNameMapper: {
    '^@core/(.*)$': '<rootDir>/src/app/core/$1',
    '^@layout/(.*)$': '<rootDir>/src/app/layout/$1',
    '^@modules/(.*)$': '<rootDir>/src/app/modules/$1',
    '^@shared/(.*)$': '<rootDir>/src/app/shared/$1',
    '^@services/(.*)$': '<rootDir>/src/app/services/$1',
    '^@environments/(.*)$': '<rootDir>/src/environments/$1',
  },
}
